#!/usr/bin/env Rscript
# would_you_rather_bot 0.1
# author: jeremy r. winget
library(tidyverse)
library(rtweet)
library(here)
library(glue)

# authenticate
token <- get_tokens()

# read in data
wyr <- read_csv(here("wyr-db.csv"), col_names = FALSE)
pictures <- list.files(here("img"))

# function to generate sentence
would_you_rather <- function() {
  choices <- sample_n(wyr, 2)
  a <- slice(choices, 1)
  b <- slice(choices, 2)
  sentence <- glue("Would you rather {a} or {b}?")
  return(sentence)
}

# generate question and picture
tweet <- would_you_rather()
img <- sample(pictures, 1)

# tweet it
post_tweet(status = glue(
  "{tweet}
  
#wouldyourather"),
  media = glue("img/{img}"))

# create log entry
line <- paste(as.character(Sys.time()), tweet, sep = " ")
write(line, file = here("wyr-tweets.log"), append = TRUE)
